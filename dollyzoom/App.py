# Vertigo (DollyZoom)

import cv2
import numpy as np
from Points import Points

from dollyzoom.Image import Image

# Temp global variables
n_points_selected = 0
points = np.empty([2, 1, 2], dtype=np.float32)
initial_resolution = [0, 0]
initial_distance = 0
initial_points = np.empty([2, 1, 2], dtype=np.float32)


# Load video & read first frame
# capture = cv2.VideoCapture('data/20170913_213752_001.mp4')
capture = cv2.VideoCapture('data/GOPR0133.MP4')
capture.set(cv2.CAP_PROP_FRAME_WIDTH, 360)
capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 640)
red, old_frame = capture.read()
old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
initial_resolution[0], initial_resolution[1], channels = old_frame.shape


# Add points to the existing array on mouse click
def sample_track_points(event, x, y, flags, param):
    global n_points_selected
    if event == cv2.EVENT_LBUTTONDOWN:
        points[n_points_selected] = [x, y]
        n_points_selected += 1

cv2.namedWindow('frame')
cv2.setMouseCallback('frame', sample_track_points)
cv2.imshow('frame', old_frame)

while n_points_selected < 2:
    cv2.waitKey(50)
initial_distance = Points.get_distance(points)
initial_points = points.copy()

# Start the processing
while True:
    ret, frame = capture.read()
    if frame is None:
        break
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Calculate optical flow
    points_new, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, points, None,
        winSize=(15, 15), maxLevel=2, criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

    # Draw dots
    for point in points:
        x, y = point.ravel()
        frame = cv2.circle(frame, (x, y), 5, (0, 0, 255), -1)

    # Zoom in image & stabilize
    zoom_coef = max(1, initial_distance / Points.get_distance(points))
    res = Image.zoom(frame, zoom_coef)
    res = Image.stabilize(res, initial_points, points, zoom_coef)
    cv2.imshow('frame', Image.resize(res))

    # Now update the previous frame and previous points
    old_gray = frame_gray.copy()
    points = points_new

    cv2.waitKey(10)

cv2.waitKey(0)
cv2.destroyAllWindows()
capture.release()
