import cv2
from dollyzoom.Config import Config
from dollyzoom.Points import Points


class Image(object):
    @staticmethod
    def zoom(initial_image, coef):
        old_h, old_w, channels = initial_image.shape
        image = cv2.resize(initial_image, None, fx=coef, fy=coef, interpolation=cv2.INTER_LINEAR)
        new_h, new_w, channels = image.shape

        crop_h = int((new_h - old_h) / 2)
        crop_w = int((new_w - old_w) / 2)

        image = image[
                crop_h:(new_h - crop_h),
                crop_w:(new_w - crop_w)]

        return image

    @staticmethod
    def stabilize(image, initial_points, points, zoom_coef):
        original_h, original_w, channels = image.shape
        initial_cw, initial_ch = Points.get_center(initial_points)
        cw, ch = Points.get_center(points)
        diff_cw = cw - initial_cw
        diff_ch = initial_ch - ch

        if diff_cw > 0:
            crop_w = min(Config.STABILIZE_CROP, int(diff_cw))
        else:
            crop_w = max(-Config.STABILIZE_CROP, int(diff_cw))

        if diff_ch > 0:
            crop_h = min(Config.STABILIZE_CROP, int(diff_ch))
        else:
            crop_h = max(-Config.STABILIZE_CROP, int(diff_ch))

        image = image[
                Config.STABILIZE_CROP + crop_h: original_h - Config.STABILIZE_CROP + crop_h,
                Config.STABILIZE_CROP + crop_w: original_w - Config.STABILIZE_CROP + crop_w]

        return image

    @staticmethod
    def resize(frame):
        h, w, channels = frame.shape
        coef = min([1, Config.MAX_HEIGHT / h, Config.MAX_WIDTH / w])
        return cv2.resize(frame, None, fx=coef, fy=coef, interpolation=cv2.INTER_LINEAR)

