import math


class Points(object):
    @staticmethod
    def get_distance(points):
        return math.sqrt(
            math.pow(points[0][0][0] - points[1][0][0], 2) + math.pow(points[0][0][1] - points[1][0][1], 2))

    @staticmethod
    def get_center(points):
        x = (points[0][0][0] + points[1][0][0]) / 2
        y = (points[0][0][1] + points[1][0][1]) / 2
        return [x, y]

